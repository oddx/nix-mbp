{ pkgs, ... }:
{
  programs.helix = {
    enable = true;
    ignores = [ "node_modules/" ];
    languages = {
      language-server = {
        haskell-language-server.command = "${pkgs.haskell-language-server}/bin/haskell-language-server-9.6.6";
        typescript-language-server = with pkgs.nodePackages; {
          command = "${typescript-language-server}/bin/typescript-language-server";
          args = [ "--stdio" ];
        };
        nil.command = "${pkgs.nil}/bin/nil";
        pylsp.command = "${pkgs.python312Packages.python-lsp-server}/bin/pylsp";
        ruff.command = "${pkgs.ruff}/bin/ruff";
        rust-analyzer.config.check.command = "clippy";
      };
      language = [
        {
          name = "haskell";
          auto-format = true;
          formatter.command = "${pkgs.ormolu}/bin/ormolu";
        }
        {
          name = "javascript";
          auto-format = true;
        }
        {
          name = "typescript";
          auto-format = true;
        }
        {
          name = "tsx";
          auto-format = true;
          formatter.command = "${pkgs.nixfmt-classic}/bin/nixfmt";
        }
        {
          name = "jsx";
          auto-format = true;
        }
        {
          name = "nix";
          auto-format = true;
        }
        {
          name = "ocaml";
          auto-format = true;
        }
        {
          name = "python";
          auto-format = true;
          formatter.command = "${pkgs.ruff}/bin/ruff";
        }
        {
          name = "rust";
          auto-format = true;
        }
      ];
    };
    settings = {
      theme = "everforest_dark";
      #theme = "everforest_light";
      #theme = "ferra";
      #theme = "github_dark_colorblind";
      #theme = "base16_default";
      editor.statusline = {
        left = ["mode" "spinner"];
        center = ["file-name"];
        right = ["diagnostics" "position" "position-percentage" "file-type"];
      };
    };
  };
}
