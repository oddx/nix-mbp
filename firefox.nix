{ config, pkgs, ... }:
{
  programs.firefox = {
    enable = true;
    # figure this out later
    # policies = {
    #   DefaultDownloadDirectory = "/home/enmesh/downloads";
    #   ExtensionSettings = {
    #       "*".installation_mode = "blocked"; # blocks all addons except the ones specified below
    #       # Bitwarden:
    #       "{446900e4-71c2-419f-a6a7-df9c091e268b}" = {
    #         install_url = "https://addons.mozilla.org/firefox/downloads/latest/bitwarden-password-manager/latest.xpi";
    #         installation_mode = "force_installed";
    #       };
    #       # LocationGuard:
    #       "	jid1-HdwPLukcGQeOSh@jetpack" = {
    #         install_url = "https://addons.mozilla.org/firefox/downloads/latest/location-guard/latest.xpi";
    #         installation_mode = "force_installed";
    #       };
    #       # Simple Translate:
    #       "simple-translate@sienori" = {
    #         install_url = "https://addons.mozilla.org/firefox/downloads/latest/simple-translate/latest.xpi";
    #         installation_mode = "force_installed";
    #       };
    #       # uBlock Origin:
    #       "uBlock0@raymondhill.net" = {
    #         install_url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
    #         installation_mode = "force_installed";
    #       };
    #     };
    #   };
      profiles.enmesh = {
      bookmarks = [
        {
          name = "General";
          toolbar = true;
          bookmarks = [
            {
              name = "nixpkgs";
              url = "https://search.nixos.org/packages";
              keyword = "nixpkgs";
            }
            {
              name = "ShareDrop";
              url = "https://www.sharedrop.io";
              keyword = "sharedrop";
            }
            {
              name = "Home Manager Options";
              url = "https://nix-community.github.io/home-manager/options.xhtml";
              keyword = "home";
            }
            {
              name = "Invidious Instances";
              url = "https://docs.invidious.io/instances/";
              keyword = "invidious";
            }
          ];
        }
      ];
      #containers = { };
      
      #extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      #  bitwarden-password-manager
      #  location-guard
      #  simple-translate
      #  #tabliss
      #  #tree-style-tab
      #  ublock-origin
      #];
      settings = {
        #"gfx.webrender.all" = true;
        #"media.ffmpeg.vaapi.enabled" = true;
        #"widget.dmabuf.force-enabled" = true;
        # maps ctrl to super
        #"ui.key.accelKey" = 91;
        #"browser.download.dir" = "/home/enmesh/downloads";
        "browser.toolbars.bookmarks.visibility" = "never";
        "browser.search.suggest.enabled" = false;
        "browser.privatebrowsing.autostart" = true;
        "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
        "browser.newtabpage.activity-stream.feeds.topsites" = false;
        "browser.disableResetPrompt" = true;
        "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
        "browser.shell.checkDefaultBrowser" = false;
        "browser.shell.defaultBrowserCheckCount" = 1;
        "browser.search.defaultenginename" = "DuckDuckGo";
        "browser.urlbar.suggest.engines" = false;
        "browser.urlbar.suggest.quicksuggest.sponsored" = false;
        #"dom.security.https_only_mode" = true;
        #"identity.fxaccounts.enabled" = false;
        "extensions.pocket.enabled" = false;
        "privacy.trackingprotection.enabled" = true;
        "signon.rememberSignons" = false;
        "sidebar.revamp" = true;
        "sidebar.verticalTabs" = true;

        #fix this overlay shit
        "media.gmp-widevinecdm.version" = "system-installed";
        "media.gmp-widevinecdm.visible" = true;
        "media.gmp-widevinecdm.enabled" = true;
        "media.gmp-widevinecdm.autoupdate" = false;
        "media.eme.enabled" = true;
        "media.eme.encrypted-media-encryption-scheme.enabled" = true;
      };
      search = {
        force = true;
        default = "DuckDuckGo";
        order = [ "DuckDuckGo" "Google" ];
      };
    };
  };

  xdg.mimeApps.defaultApplications = {
    "text/html" = [ "firefox.desktop" ];
    "text/xml" = [ "firefox.desktop" ];
    "x-scheme-handler/http" = [ "firefox.desktop" ];
    "x-scheme-handler/https" = [ "firefox.desktop" ];
  };
}
