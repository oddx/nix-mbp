{
  description = "enmesh's asahi configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # hyprland-plugins.url = "github:hyprwm/hyprland-plugins";
    # hyprland.url = "github:hyprwm/Hyprland?rev=12f9a0d0b93f691d4d9923716557154d74777b0a";
    nixos-apple-silicon.url = "github:tpwrules/nixos-apple-silicon";
    ags.url = "github:Aylur/ags?rev=bb91f7c8fdd2f51c79d3af3f2881cacbdff19f60";
    # anyrun = {
    #   url = "github:anyrun-org/anyrun";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
  };

  outputs = inputs @ {
    ags,
    # anyrun,
    nixpkgs,
    home-manager,
    # hyprland,
    nixos-apple-silicon,
    ...
  }: let
    # Add inputs to module option
    inputsModule = {lib, ...}: {
      options.roadrunner.inputs = lib.mkOption {
        type = with lib.types; attrsOf unspecified;
        default = {};
      };
      config.roadrunner.inputs = inputs;
    };
    system = "aarch64-linux";
  in 
  {
    formatter.aarch64-linux = nixpkgs.legacyPackages.${system}.alejandra;
    nixosConfigurations.roadrunner = nixpkgs.lib.nixosSystem {
      system = "aarch64-linux";
      modules = [
        inputsModule
        ./configuration.nix
        nixos-apple-silicon.nixosModules.apple-silicon-support
        home-manager.nixosModules.home-manager
        # hyprland.nixosModules.default
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.enmesh.imports = [
            ./home.nix
            inputsModule
            ags.homeManagerModules.default
            # anyrun.homeManagerModules.default
            # hyprland.homeManagerModules.default
          ];
        }
      ];
    };
  };
}
