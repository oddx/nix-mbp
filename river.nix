{ pkgs, ... }:
{
  wayland.windowManager.river = {
     enable = true;
     package = null;
     # TODO: Overlay custom settings on default config
     # extraConfig = builtins.readFile ./river.config;
     extraSessionVariables = {
        MOZ_ENABLE_WAYLAND = "1";
     };
  };
}
