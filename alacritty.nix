{ pkgs, ... }:
{
  programs.alacritty = {
    enable = true;
    settings = {
      colors = {
        primary = {
           background = "#1c2023";
           foreground = "#c7ccd1";
        };
        normal = {
          black = "#1c2023";
          red = "#c7ae95";
          green = "#95c7ae";
          yellow = "#aec795";
          blue = "#ae95c7";
          magenta = "#c795ae";
          cyan = "#95aec7";
          white = "#c7ccd1";
        };
        bright = {
          black = "#747c84";
          red = "#c7ae95";
          green = "#95c7ae";
          yellow = "#aec795";
          blue = "#ae95c7";
          magenta = "#c795ae";
          cyan = "#95aec7";
          white = "#f3f4f5";
        };
      };
      window = {
        dimensions = {
          columns = 96;
          lines = 16;
        };
        padding = {
          x = 10;
          y = 10;
        };
      };
      font = {
        normal = {
          family = "FiraCode";
        };
        size = 12;
      };
      cursor = {
        style = {
          blinking = "On";
        };
      };
    };
  };
}
