{
  programs.nushell = {
    enable = true;
    extraConfig = ''
      # set -o emacs

      # fmt -w 80 ~/.motd.txt

      # PS1='%B%F{cyan}%m:%b%F{white}%1~ %B%F{cyan}λ%f%F{magenta}=%f%b '

      let displayExists = $env.DISPLAY? | default "" | $in == ""
      let isTTY1 = (tty) == '/dev/tty1'

      if displayExists and isTTY1 {
        exec Hyprland
      }
    '';
    extraLogin = ''
      
    '';
    shellAliases = {
      streamup = "gphoto2 --stdout --capture-movie | ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -f v4l2 /dev/video0";
      rip = "yt-dlp -f bestaudio -x --audio-format mp3 --audio-quality 0 --add-metadata --embed-thumbnail -o \"%(artist)\"";

      vi = "hx";
      vim = "hx";
      nano = "hx";
      # ls = "ls -aFhl";
      # motd = "$VISUAL $HOME/.motd.txt";
      # todo = "$VISUAL $HOME/.todo.md";
      # vi = "$VISUAL";
    };
    configFile = { text = ''
      let $config = {
        show_banner: false
      }
    '';
    };
  };
}
