import GLib from 'gi://GLib';
import Weather from '../../services/weather.js'

const DesktopWeather = () => Widget.Label({
  name: 'weatherMain',
  className: 'weatherMain',
  label: Weather.bind('forecasts')
    .as(f => f[0] ? formatCond(f) : '')
})

const WeatherDetails = () => Widget.Box({
  name: 'weatherDetails',
  className: 'weatherDetails',
  child: Widget.CenterBox({
    start_widget: Widget.Label({
      name: 'temperature',
      className: 'temperature',
      hpack: 'start',
      label: Weather.bind('forecasts')
        .as(f => f[0] ? formatTemp(f) : '')
    }),
    center_widget: Widget.Box({
      class_name: 'separator-box',
      vertical: true,
      hexpand: true,
      hpack: 'center',
      children: [
        Widget.Separator({ vpack: 'center', vexpand: true }),
        Widget.Label({
          name: 'humidity',
          className: 'humidity',
          hpack: 'end',
          label: Weather.bind('forecasts')
            .as(f => f[0] ? formatHum(f) : '')
        }),
        Widget.Separator({ vpack: 'center', vexpand: true }),
      ],
    }),
    end_widget: Widget.Label({
      name: 'sun',
      className: 'sun',
      hpack: 'end',
      label: Weather.bind('forecasts')
        .as(f => f[0] ? formatSun(f) : '')
    })
  }),
})

const formatTemp = (t) => `${Math.round(t[0].current.temp)}°`
const formatHum = (t) => `${t[0].current.humidity}%`
const formatCond = (t) => `${t[0].current.weather[0].main}`
const formatSun = (t) => {
  const uvi = t[0].current.uvi
  const sunrise = t[0].current.sunrise
  const sunset = t[0].current.sunset
  const next_sunrise = t[0].daily[1].sunrise

  if (uvi > 3) {
    return `${uvi.toFixed(1)}`
  } else {
    if (parseInt(clock.bind('value').transform(time => time.format('%H'))) < 12) {
      return `${unixTimestampToLocal(sunrise)}`
    } else if (clock.value > sunset) {
      return `${unixTimestampToLocal(next_sunrise)}`
    } else {
      return `${unixTimestampToLocal(sunset)}`
    }
  }
}

const unixTimestampToLocal = (ts) => {
  const local = new Date(ts * 1000)
  return `${toTwelveHour(local.getHours())}:${local.getMinutes()}`
}

const toTwelveHour = (h) => (h + 11) % 12 + 1

const clock = Variable(GLib.DateTime.new_now_local(), {
  poll: [1000, () => GLib.DateTime.new_now_local()],
});

const Clock = ({
  format = '%A %b %e %H:%M',
  ...rest
} = {}) => Widget.Label({
  label: clock.bind('value').transform(time => {
    return time.format(format) || 'wrong format';
  }),
  ...rest,
});

const DesktopClock = () => Widget.Box({
  class_name: 'clock-box',
  child: Widget.CenterBox({
    class_name: 'clock-time',
    start_widget: Clock({
      class_name: 'clock-hour',
      hpack: 'center',
      format: '%I',
    }),
    center_widget: Widget.Box({
      class_name: 'separator-box',
      vertical: true,
      hexpand: true,
      hpack: 'center',
      children: [
        Widget.Separator({ vpack: 'center', vexpand: true }),
        Widget.Label({ label: ':', className: 'clock-separator' }),
        Widget.Separator({ vpack: 'center', vexpand: true }),
      ],
    }),
    end_widget: Clock({
      class_name: 'clock-minute',
      hpack: 'center',
      format: '%M',
    }),
  }),
});

const Desktop = () => Widget.Box({
  children: [
    Widget.Box({
      className: 'weather-widget',
      vertical: true,
      vexpand: true,
      hexpand: true,
      vpack: 'end',
      hpack: 'start',
      css: `margin: 36px`,
      children: [
        WeatherDetails(),
        DesktopWeather(),
      ],
    }),
    Widget.Box({
      className: 'clock-widget',
      vertical: true,
      vexpand: true,
      hexpand: true,
      vpack: 'end',
      hpack: 'end',
      css: `margin: 36px`,
      children: [
        DesktopClock(),
        Clock({ format: '%A, %B %e', class_name: 'clock-date' }),
      ],
    })
  ],
})

/** @param {number} monitor */
export default monitor => Widget.Window({
  monitor,
  keymode: 'none',
  name: `desktop${monitor}`,
  layer: 'background',
  class_name: 'desktop',
  anchor: ['top', 'bottom', 'left', 'right'],
  child: Desktop(),
});
