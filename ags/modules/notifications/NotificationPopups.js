import Notification from "./Notification.js"

const config = (notifs) => {
  notifs.popupTimeout = 7500
  notifs.forceTimeout = true
  return notifs
}

const notifications = config(await Service.import("notifications"))
const transition = 200
const position = ['top', 'right']
const { timeout, idle } = Utils

const dismissable = (notification) =>
  Object.assign(notification, {
    dismiss: () => notification.destroy(),
  })

function PopupList() {
  const map = new Map
  const box = Widget.Box({
    hpack: "end",
    vertical: true,
    css: `min-width: 28rem;`,
  })

  function remove(_, id) {
    map.get(id)?.dismiss()
    map.delete(id)
  }

  return box
    .hook(notifications, (_, id) => {
      if (id !== undefined) {
        if (map.has(id))
          remove(null, id)

        if (notifications.dnd)
          return

        // const w = Animated(id)
        const notification = dismissable(
          Notification(notifications.getNotification(id))
        )
        map.set(id, notification)
        box.children = [notification, ...box.children]
      }
    }, "notified")
    .hook(notifications, remove, "dismissed")
    .hook(notifications, remove, "closed")
}

export default (monitor) => Widget.Window({
  monitor,
  name: `notifications${monitor}`,
  anchor: position,
  class_name: "notifications",
  child: Widget.Box({
    css: "padding: 2px;",
    child: PopupList(),
  }),
})
