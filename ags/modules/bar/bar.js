const Battery = await Service.import('battery');
const Bluetooth = await Service.import("bluetooth");
const Network = await Service.import("network");
const { exec, execAsync } = Utils;

// Widgets
import { Workspaces } from "./workspaces.js";

const CLICK_COUNT_RIGHT = Variable(0)
const CLICK_COUNT_LEFT = Variable(0)
const HOUR = Variable(exec('date "+%H"'), {
  poll: [1000 * 60 * 15, 'date "+%H"'],
})
const ISDND = Variable(exec('makoctl mode'))
const ISLISTING = Variable(false)

const isNight = () => HOUR.value >= 20 || HOUR.value <= 5

const NetworkIndicator = () => Widget.Icon().hook(Network, self => {
  const icon = Network[Network.primary || "wifi"]?.icon_name
  self.icon = icon || ""
  self.visible = !!icon
})

const BluetoothWidget = () =>
  Widget.Label({
    className: "bluetoothIcon",
    label: "󰂲",
    setup: (self) => {
      self.hook(Bluetooth, (self) => {
        if (Bluetooth.enabled) {
          self.className = "barBluetoothIcon";
          self.label = "󰂯";
        } else {
          self.className = "barBluetoothIcon off";
          self.label = "󰂲";
        }
      });
    },
  });

const divide = ([total, free]) => free / total

//find better cpu calculation
const cpu = Variable(0, {
  poll: [1000 * 5, 'top -b -n 1', out => divide([100, out.split('\n')
    .find(line => line.includes('Cpu(s)'))
    .split(/\s+/)[1]
    .replace(',', '.')])],
})

const temp = Variable(0, {
  poll: [1000 * 5, `cat /sys/class/thermal/thermal_zone0/temp`, n => {
    return ((Number.parseInt(n) / 100_000) - .28) * 2.5
  }],
})

const cpuIndicator = () => Widget.Box({
  tooltipText: cpu.bind('value').as(v => `${Math.ceil(v * 100)}%`),
  child:
    Widget.CircularProgress({
      value: cpu.bind(),
      css: 'min-width: 21.5px;'  // its size is min(min-height, min-width)
        + 'min-height: 21.5px;'
        + 'font-size: 1.5px;' // to set its thickness set font-size on it
        + 'margin: 3px;' // you can set margin on it
        + 'background-color: #131313;' // set its bg color
        + 'color: aqua;', // set its fg color
      startAt: 0.75,

    }),
  setup: self => self.hook(CLICK_COUNT_RIGHT, () => {
    self.visible = CLICK_COUNT_RIGHT.value === 1 || CLICK_COUNT_RIGHT.value === 2
  })
})

// const ramIndicator = () => Widget.CircularProgress({
//   value: ram.bind(),
//   css: 'min-width: 21.5px;'
//     + 'min-height: 21.5px;'
//     + 'font-size: 1.5px;'
//     + 'margin: 3px;'
//     + 'background-color: #131313;'
//     + 'color: aqua;',
//   startAt: 0.75,
//   setup: self => self.hook(CLICK_COUNT_RIGHT, () => {
//     self.visible = CLICK_COUNT_RIGHT.value === 2 || CLICK_COUNT_RIGHT.value === 3
//   }),
// })

const tempIndicator = () => Widget.Box({
  tooltipText: temp.bind('value').as(v => `${Math.ceil(v * 100)}%`),
  child:
    Widget.CircularProgress({
      value: temp.bind(),
      css: 'min-width: 21.5px;'
        + 'min-height: 21.5px;'
        + 'font-size: 1.5px;'
        + 'margin: 3px;'
        + 'background-color: #131313;'
        + 'color: fuchsia;',
      startAt: 0.75,

    }),
  setup: self => self.hook(CLICK_COUNT_RIGHT, () => {
    self.visible = CLICK_COUNT_RIGHT.value === 2 || CLICK_COUNT_RIGHT.value === 3
  })
})

const cycleSystemInfo = () => {
  CLICK_COUNT_RIGHT.value =
    CLICK_COUNT_RIGHT.value < 3 ?
      CLICK_COUNT_RIGHT.value + 1 :
      0
}

// default battery bar
// figure out charging styling
// const BatteryWidget = () =>
//   Widget.LevelBar({
//     bar_mode: 'discrete',
//     class_name: `barBattery ${Battery.bind('charging').as(ch => ch ? 'charging' : '')}`,
//     max_value: 5,
//     value: Battery.bind("percent").as(p => p > 0 ? p / 20 : 0),
//     visible: Battery.bind('available'),
//     widthRequest: 100,
//   }) 

// maybe add variations e.g. halloween, mondays, etc
// 󰚌 󰍳 󰛞 󰋕 󰋑 󰋘 󰋙 󰖔
const BatteryIcon = (value, icon, inverse) =>
  value > 79 ? `${icon} ${icon} ${icon} ${icon} ${icon}` :
    value > 59 ? `${icon} ${icon} ${icon} ${icon} ${inverse}` :
      value > 39 ? `${icon} ${icon} ${icon} ${inverse} ${inverse}` :
        value > 19 ? `${icon} ${icon} ${inverse} ${inverse} ${inverse}` :
          `${icon} ${inverse} ${inverse} ${inverse} ${inverse}`

// const timeRemaining = (t) =>
//   `${(t / (60 * 60)).toFixed(0)}:${(t % 60).toString().padStart(2, 0)}`


const cycleBatteryInfo = () => {
  CLICK_COUNT_LEFT.value =
    CLICK_COUNT_LEFT.value < 3 ?
      CLICK_COUNT_LEFT.value + 1 :
      0
}

const CustomBatteryIndicator = () =>
  Widget.Box({
    hpack: 'center',
    children: [
      Widget.Label({
        className: 'customBarBattery',
        label: Battery.bind('percent').as(p =>
          BatteryIcon(p,
            isNight() ? '󰖔' : '󰚌',
            '󰋙'
          )
        ),
        setup: self => self.hook(Battery, () => {
          self.label = BatteryIcon(Battery.percent,
            isNight() ? '󰖔' : '󰚌',
            '󰋙'
          )

          self.className = Battery.charging ? isNight() ? 'customBarBattery chargingNight' :
            'customBarBattery charging' :
            Battery.percent <= 10 ? 'customBarBattery low' :
              'customBarBattery'
        }),
      }),
      Widget.Label({
        name: 'batteryPercent',
        label: Battery.bind('percent').as(p => `${p}%`),
        setup: self => self.hook(CLICK_COUNT_LEFT, () => {
          self.visible = CLICK_COUNT_LEFT.value === 1 || CLICK_COUNT_LEFT.value === 2
        }),
      }),
      Widget.Label({
        name: 'batteryDraw',
        label: Battery.bind('energy-rate').as(w => `${w.toFixed(2)}W`),
        setup: self => self.hook(CLICK_COUNT_LEFT, () => {
          self.visible = CLICK_COUNT_LEFT.value === 2 || CLICK_COUNT_LEFT.value === 3
        }),
      }),
    ]
  })

const cycleDND = () => {
  if (ISDND.value.includes('dnd')) {
    exec('makoctl mode -r dnd')
  } else {
    exec('makoctl mode -s dnd')
  }
  ISDND.value = exec('makoctl mode')
}

// remember to search icon names by system theme
// e.g. adwaita
const Dnd = () =>
  Widget.Button({
    className: 'dnd',
    onPrimaryClick: toggleNotificationList,
    onSecondaryClick: cycleDND,
    child: Widget.Icon({
      name: 'dnd',
      setup: self => self.hook(ISDND, () => {
        self.icon = ISDND.value.includes('dnd')
          ? "notifications-disabled-symbolic"
          : "preferences-system-notifications-symbolic"
      }),
      cursor: "pointer",
    })
  })

const toggleNotificationList = () => {
  if (!ISLISTING.value) {
    JSON.parse(exec('makoctl history'))
      .data?.flat()
      .forEach(_ => exec('makoctl restore'))
  } else {
    exec('makoctl dismiss -a')
  }
  ISLISTING.value = !ISLISTING.value
}

// const NotificationList = () =>
//   Widget.Button({
//     className: 'notification-list-label',
//     onPrimaryClick: toggleNotificationList,
//     child: Widget.Icon({
//       name: 'notification-list-icon',
//       hpack: 'center',
//       vpack: 'center',
//       icon: 'view-list-symbolic',
//     }),
//     // child: Widget.Label({
//     //   label: "≡",
//     //   hpack: 'center',
//     //   vpack: 'center',
//     // }),
//     cursor: "pointer",
//   })

const Date = () =>
  Widget.Label({
    className: "dateLabel",
    setup: (self) => {
      // self.poll(1000, (self) => (self.label = exec('date "+%a %b %e"')));
      self.poll(1000, (self) =>
        execAsync(["date", "+%a %b %e"])
          .then((time) => (self.label = time))
          .catch(console.error),
      );
    },
  });

const Time = () =>
  Widget.Label({
    className: "timeLabel",
    setup: (self) => {
      // self.poll(1000, (self) => (self.label = exec('date "+%l:%M"')));
      self.poll(1000, (self) =>
        execAsync(["date", "+%l:%M"])
          .then((time) => (self.label = time))
          .catch(console.error),
      );
    },
  });

const Clock = () =>
  Widget.Box({
    className: "clock",
    children: [
      Widget.Button({
        onSecondaryClick: cycleSystemInfo,
        child: Widget.Box({
          children: [Date(), Time()]
        }),
      }),
    ],
  });

const Left = () =>
  Widget.Box({
    hpack: "start",
    className: "bar-left",
    children: [
      Widget.Button({
        className: "systemInfo",
        onPrimaryClick: () => App.toggleWindow("controlPanel"),
        onSecondaryClick: cycleBatteryInfo,
        child: Widget.Box({
          children: [NetworkIndicator(), BluetoothWidget(), CustomBatteryIndicator()],
        }),
      }),
    ],
  });

const Center = () =>
  Widget.Box({
    children: [Workspaces()],
    className: "bar-center",
  });

const Right = () =>
  Widget.Box({
    hpack: "end",
    children: [tempIndicator(), cpuIndicator(), Dnd(), /* NotificationList() */, Clock()],
    className: "bar-right",
  });

export const Bar = () =>
  Widget.Window({
    name: "bar",
    anchor: ["top", "right", "left"],
    exclusivity: "exclusive",
    margins: [8, 8, 0],
    child: Widget.CenterBox({
      className: "bar",
      start_widget: Left(),
      center_widget: Center(),
      end_widget: Right(),
    }),
  });
