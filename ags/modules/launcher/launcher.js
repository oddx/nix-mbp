const Applications = await Service.import("applications");
const Hyprland = await Service.import("hyprland");
import PopupWindow from "../../utils/popupWindow.js";
// const { Gdk } = imports.gi;

const WINDOW_NAME = "launcher";

const bash = (strings, ...values) => {
  const cmd = typeof strings === "string" ? strings : strings
    .flatMap((str, i) => str + `${values[i] ?? ""}`)
    .join("")

  return Utils.execAsync(["bash", "-c", cmd]).catch(err => {
    console.error(cmd, err)
    return ""
  })
}

const truncateString = (str, maxLength) =>
  str.length > maxLength ? `${str.slice(0, maxLength)}...` : str;

const launchApp = (app) => {
  const exe = app.executable
    .split(/\s+/)
    .filter(str => !str.startsWith("%") && !str.startsWith("@"))
    .join(" ")

  bash(`${exe} &`)
  app.frequency += 1
}

const AppItem = (app) =>
  Widget.Button({
    className: "launcherApp",
    onClicked: () => {
      App.closeWindow(WINDOW_NAME);
      launchApp(app)
      ++app.frequency;
    },
    setup: (self) => (self.app = app),
    child: Widget.Box({
      className: "launcherListing",
      children: [
        Widget.Icon({
          className: "launcherItemIcon",
          icon: app.iconName || "",
          size: 24,
        }),
        Widget.Box({
          className: "launcherItem",
          vertical: true,
          vpack: "center",
          children: [
            Widget.Label({
              className: "launcherItemTitle",
              label: app.name,
              xalign: 0,
              vpack: "center",
              truncate: "end",
            }),
            /*!!app.description &&
              Widget.Label({
                className: "launcherItemDescription",
                label: truncateString(app.description, 75) || "",
                wrap: true,
                xalign: 0,
                justification: "left",
                vpack: "center",
              }),*/
          ],
        }),
      ],
    }),
  });

const Launcher = () => {
  const list = Widget.Box({
    children: Applications.list.map(AppItem),
    vertical: true,
  });

  const entry = Widget.Entry({
    className: "launcherEntry",
    hexpand: true,
    text: "",
    onAccept: ({ text }) => {
      const isCommand = text.startsWith(">");
      const appList = Applications.query(text || "");
      if (isCommand === true) {
        App.toggleWindow(WINDOW_NAME);
        Utils.execAsync(text.slice(1));
      } else if (appList[0]) {
        App.toggleWindow(WINDOW_NAME);
        launchApp(appList[0]);
      }
    },
    onChange: ({ text }) =>
      list.children.map((item) => {
        item.visible = item.app.match(text);
      }),
  });

  return Widget.Box({
    className: "launcher",
    vertical: true,
    setup: (self) => {
      self.hook(App, (_, name, visible) => {
        if (name !== WINDOW_NAME) return;
        if (visible) entry.grab_focus();
      });
    },
    children: [
      entry,
      Widget.Scrollable({
        hscroll: "never",
        child: list,
        className: "launcherItems",
      }),
    ],
  });
};

export const launcher = PopupWindow({
  name: WINDOW_NAME,
  // anchor: ["center"],
  //exclusivity: 'exclusive',
  layer: "overlay",
  margins: [12],
  transition: "slide_down",
  transitionDuration: 150,
  // popup: true,
  keymode: "on-demand",
  child: Launcher(),
});
