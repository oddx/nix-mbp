import { opened } from "./utils.js"

export const Menu = ({ name, icon, title, content }) => Widget.Revealer({
  transition: "slide_down",
  reveal_child: opened.bind().as(v => v === name),
  child: Widget.Box({
    class_names: ["menu", name],
    vertical: true,
    children: [
      Widget.Box({
        vertical: true,
        class_name: "content vertical",
        children: content,
      }),
    ],
  }),
})
