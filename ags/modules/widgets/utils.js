export const opened = Variable("")
App.connect("window-toggled", (_, name, visible) => {
  if (name === "quicksettings" && !visible)
    Utils.timeout(500, () => opened.value = "")
})

