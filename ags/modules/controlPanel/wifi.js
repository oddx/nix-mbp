const Network = await Service.import("network");

import { ArrowToggleButton } from "../widgets/Arrow.js";
import { Menu } from "../widgets/Menu.js";

export const NetworkToggle = () => ArrowToggleButton({
  name: "network",
  className: "network-toggle",
  icon: Network.wifi.bind("icon_name"),
  label: Network.wifi.bind("ssid").as(ssid => ssid || "Not Connected"),
  connection: [Network.wifi, () => Network.wifi.enabled],
  deactivate: () => Network.wifi.enabled = false,
  activate: () => {
    Network.wifi.enabled = true
  },
})

export const WifiSelection = () => Menu({
  name: "network",
  className: "network",
  icon: Network.wifi.bind("icon_name"),
  title: "Wifi Selection",
  content: [
    Widget.Box({
      vertical: true,
      setup: self => self.hook(Network.wifi, () => self.children =
        Network.wifi.access_points.map(ap => Widget.Button({
          on_clicked: () => {
            Utils.execAsync(`iwctl station wlan0 connect ${ap.ssid}`)
          },
          child: Widget.Box({
            children: [
              Widget.Icon(ap.iconName),
              Widget.Label(ap.ssid || ""),
              Widget.Icon({
                icon: 'object-select-symbolic',
                hexpand: true,
                hpack: "end",
                setup: self => Utils.idle(() => {
                  if (!self.is_destroyed)
                    self.visible = ap.active
                }),
              }),
            ],
          }),
        })),
      ),
    }),
    // Widget.Separator(),
    // Widget.Button({
    //   on_clicked: () => sh(options.quicksettings.networkSettings.value),
    //   child: Widget.Box({
    //     children: [
    //       Widget.Icon(icons.ui.settings),
    //       Widget.Label("Network"),
    //     ],
    //   }),
    // }),
  ],
})
