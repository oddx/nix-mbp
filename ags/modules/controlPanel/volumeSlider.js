const Audio = await Service.import("audio");
import { Menu } from '../widgets/Menu.js'
import { Arrow } from '../widgets/Arrow.js';

const Icon = () =>
  Widget.Label({
    className: "volIcon",
    setup: (self) => {
      self.hook(
        Audio,
        (self) => {
          if (!Audio.speaker) return;
          // const icons = ["󰝟", "󰕿", "󰖀", "󰕾"];
          self.label = organicLevel(Audio.speaker.volume * 100)
        },
        "speaker-changed",
      );
    },
  });

const organicLevel = (n) =>
  n < 1 ? "󰝟" :
    n < 33 ? "󰕿" :
      n < 66 ? "󰖀" :
        "󰕾"

const Slider = () =>
  Widget.Slider({
    className: "volSlider",
    drawValue: false,
    onChange: ({ value }) => (Audio.speaker.volume = value),
    setup: (self) => {
      self.hook(
        Audio,
        (self) => {
          if (!Audio.speaker) return;

          self.value = Audio.speaker.volume;
        },
        "speaker-changed",
      );
    },
  });

export const VolumeSlider = () =>
  Widget.Box({
    className: "volumeSlider",
    vertical: true,
    children: [
      Widget.Label({
        className: "volLabel",
        label: "Volume",
        hpack: "start",
      }),
      Widget.Box({
        children: [Icon(), Slider(), Volume()],
      }),
    ],
  });

export const SinkSelector = () => Menu({
  name: "sink-selector",
  icon: "audio-headphones-symbolic",
  title: "Sink Selector",
  content: [
    Widget.Box({
      vertical: true,
      children: Audio.bind("speakers").as(a => a.map(SinkItem)),
    }),
    // Widget.Separator(),
    // SettingsButton(),
  ],
})

const SinkItem = (stream) => Widget.Button({
  hexpand: true,
  on_clicked: () => Audio.speaker = stream,
  child: Widget.Box({
    children: [Widget.Icon({
      icon: /* stream.icon_name || */ 'audio-x-generic-symbolic',
      tooltip_text: stream.icon_name || "",
    }),
    Widget.Label((stream.description || "").split(" ").slice(0, 4).join(" ")),
    Widget.Icon({
      icon: "object-select-symbolic",
      hexpand: true,
      hpack: "end",
      visible: Audio.speaker.bind("stream").as(s => s === stream.stream),
    }),
    ],
  }),
})

export const Volume = () => Widget.Box({
  class_name: "volume",
  hexpand: true,
  hpack: 'end',
  children: [
    Widget.Box({
      vpack: "center",
      child: Arrow("sink-selector"),
    }),
  ],
})
