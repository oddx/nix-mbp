const Bluetooth = await Service.import("bluetooth");
import { Menu } from "../widgets/Menu.js"
import { ArrowToggleButton } from "../widgets/Arrow.js"

export const BluetoothToggle = () => ArrowToggleButton({
  name: "bluetooth",
  className: 'bluetooth-toggle',
  icon: Bluetooth.bind("enabled").as(p => p ? "bluetooth-active-symbolic" : "bluetooth-disabled-symbolic"),
  label: Utils.watch("Disabled", Bluetooth, () => {
    if (!Bluetooth.enabled)
      return "Disabled"

    if (Bluetooth.connected_devices.length === 1)
      return Bluetooth.connected_devices[0].alias

    return `${Bluetooth.connected_devices.length} Connected`
  }),
  connection: [Bluetooth, () => Bluetooth.enabled],
  deactivate: () => Bluetooth.enabled = false,
  activate: () => Bluetooth.enabled = true,
})

const DeviceItem = (device) => Widget.Box({
  children: [
    Widget.Icon(device.icon_name + "-symbolic"),
    Widget.Label(device.name),
    Widget.Label({
      label: `${device.battery_percentage}%`,
      visible: device.bind("battery_percentage").as(p => p > 0),
    }),
    Widget.Box({ hexpand: true }),
    Widget.Spinner({
      active: device.bind("connecting"),
      visible: device.bind("connecting"),
    }),
    Widget.Switch({
      active: device.connected,
      visible: device.bind("connecting").as(p => !p),
      setup: self => self.on("notify::active", () => {
        device.setConnection(self.active)
      }),
    }),
  ],
})

export const BluetoothDevices = () => Menu({
  name: "bluetooth",
  className: 'bluetooth',
  icon: "bluetooth-disabled-symbolic",
  title: "Bluetooth",
  content: [
    Widget.Box({
      class_name: "bluetooth-devices",
      hexpand: true,
      vertical: true,
      children: Bluetooth.bind("devices").as(ds => ds
        .filter(d => d.name)
        .map(DeviceItem)),
    }),
  ],
})
