// Windows
import { Bar } from "./modules/bar/bar.js";
import { ControlPanel } from "./modules/controlPanel/controlPanel.js";
import { launcher } from "./modules/launcher/launcher.js";
import { Popups } from "./modules/popups/popups.js";
import NotificationPopups from "./modules/notifications/NotificationPopups.js"
import Desktop from './modules/desktop/Desktop.js'

// Apply css
const applyScss = () => {
  // Compile scss
  Utils.exec(
    `sassc ${App.configDir}/scss/main.scss ${App.configDir}/style.css`,
  );
  console.log("Scss compiled");

  // Apply compiled css
  App.resetCss();
  App.applyCss(`${App.configDir}/style.css`);
  console.log("Compiled css applied");
};

// Apply css then check for changes
applyScss();

// Check for any changes
Utils.monitorFile(`${App.configDir}/scss`, () => applyScss);

// Main config
App.config({
  style: `${App.configDir}/style.css`,
  closeWindowDelay: {
    controlPanel: 150,
    launcher: 150,
    terminal: 150,
  },
  windows: [Bar(), ControlPanel(), /* NotificationPopups(), */ launcher, Popups(), /* Desktop() */],
})
