class Brightness extends Service {
  static {
    Service.register(
      this,
      {
        'brightnessChanged': ['float']
      },
      {
        'screen': ["float", "rw"],
      },
    );
  }

  _screen = 0;
  _max = Number(Utils.exec('brightnessctl max'));
  _interface = Utils.exec("sh -c 'ls -w1 /sys/class/backlight | head -1'");

  get screen() {
    return this._screen;
  }

  set screen(percent) {
    if (percent < 0) percent = 0;

    if (percent > 1) percent = 1;

    Utils.execAsync(`brightnessctl s ${percent * 100}% -q`)
      .then(() => {
        this._screen = percent;
        this.changed("screen");
      })
      .catch(console.error);
  }

  constructor() {
    super();
    try {
      this._screen =
        Number(Utils.exec("brightnessctl g")) /
        Number(Utils.exec("brightnessctl m"));

      const brightness = `/sys/class/backlight/${this._interface}/brightness`;
      Utils.monitorFile(brightness, () => this.onChange());

      this.onChange()
    } catch (error) {
      console.error("missing dependancy: brightnessctl");
    }
  }


  onChange() {
    this._screen = Number(Utils.exec('brightnessctl get')) / this._max;

    // signals have to be explicitly emitted
    this.emit('changed'); // emits "changed"
    this.notify('screen'); // emits "notify::screen"

    // or use Service.changed(propName: string) which does the above two
    // this.changed('screen-value');

    // emit screen-changed with the percent as a parameter
    this.emit('brightnessChanged', this._screen);
  }

  // overwriting the connect method, let's you
  // change the default event that widgets connect to
  connect(event = 'brightnessChanged', callback) {
    return super.connect(event, callback);
  }
}

const service = new Brightness();

globalThis.brightness = service;

export default service;
