const { GLib } = imports.gi;
const Network = await Service.import("network");

const uint8decoder = new TextDecoder()
const key = uint8decoder.decode(
  await GLib.file_get_contents('/etc/openweather/api.key')[1]
).replace('\n', '')
const locations = JSON.parse(
  uint8decoder.decode(
    await GLib.file_get_contents('/etc/openweather/locations.json')[1]
  )
)

const [
  interval, // 1000 * 60
  cities, // []
  unit // "metric" | "imperial" | "standard"
] = [
    1000 * 60 * 30,
    [locations.manlius],
    ['imperial']
  ]

class Weather extends Service {
  static {
    Service.register(this, {}, {
      "forecasts": ["jsobject"],
    })
  }

  #forecasts = []
  get forecasts() { return this.#forecasts }
  #fetch = async ({ lat, lon }) =>
    await Utils.fetch(
      `https://api.openweathermap.org/data/3.0/onecall?lat=${lat}&lon=${lon}&exclude=minutely,hourly,alerts&appid=${key}&units=${unit}`
    ).then(res => res.json())
      .catch(console.log)

  constructor() {
    super()
    if (!key)
      return this



    Utils.interval(interval, () => {

      Network.wifi.bind('state').as(s => {
        console.log(s)
        return s === 'connected'
      })
      Promise.all(cities.map(this.#fetch)).then(forecasts => {
        this.#forecasts = forecasts
        this.changed("forecasts")
      })
    })
  }
}

export default new Weather
