{
   config,
   pkgs,
   ...
}: {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # figure this out
      # ./wireguard.nix
    ];

   # silicon-isms
   hardware.asahi = {
     experimentalGPUInstallMode = "replace";
     peripheralFirmwareDirectory = ./firmware;
     setupAsahiSound = true;
     useExperimentalGPUDriver = true;
     withRust = true;
   };

   boot = {
     # silicon-isms
     loader = {
       systemd-boot.enable = true;
       efi.canTouchEfiVariables = false;
     };

     # zfs support
     #kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
     zfs.removeLinuxDRM = true;
     supportedFilesystems = [ "exfat" "zfs" ];

     # enable camera feed
     extraModulePackages = with config.boot.kernelPackages; [
       ddcci-driver
       v4l2loopback.out
     ];
     kernelModules = [
       "v4l2loopback"
     ];
     extraModprobeConfig = ''
       options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
     '';

   };
   networking = {
     hostId = "15809f90";
     hostName = "roadrunner"; # Define your hostname.
     networkmanager.enable = true;
     networkmanager.wifi.backend = "iwd";
     wireguard.enable = true;
     wireless.iwd.enable = true;
   };
   i18n.defaultLocale = "en_US.UTF-8";
   i18n.extraLocaleSettings = {
     LC_ADDRESS = "en_US.UTF-8";
     LC_IDENTIFICATION = "en_US.UTF-8";
     LC_MEASUREMENT = "en_US.UTF-8";
     LC_MONETARY = "en_US.UTF-8";
     LC_NAME = "en_US.UTF-8";
     LC_NUMERIC = "en_US.UTF-8";
     LC_PAPER = "en_US.UTF-8";
     LC_TELEPHONE = "en_US.UTF-8";
     LC_TIME = "en_US.UTF-8";
   };

   hardware = {
     bluetooth.enable = true;
     graphics.enable = true;
     keyboard.qmk.enable = true;
     i2c.enable = true;
   };

   security = {
     rtkit.enable = true;
     sudo.enable = false;
     doas.enable = true;
     doas.extraRules = [{
       users = [ "enmesh" ];
       keepEnv = true;
       persist = true;
     }];
     pam.services = {
       hyprlock = {};
       login.enableGnomeKeyring = true;
       swaylock = {};
     };
     polkit = {
       enable = true;
       extraConfig = ''
         // nautilus internal storage mounting
         polkit.addRule(function(action, subject) {
           if(subject.user === "enmesh"
             && action.id === "org.freedesktop.udisks2.filesystem-mount-system") {
             return "yes";
           }
         });
       '';
     };
   };

   services = {
     automatic-timezoned.enable = true;
     blueman.enable = true;
     chrony.enable = true;
     geoclue2.geoProviderUrl = "https://beacondb.net/v1/geolocate";
     gnome.gnome-keyring.enable = true;
     gvfs.enable = true; # gnome files
     localtimed.enable = true;
     logind = {
       lidSwitchExternalPower = "ignore";
       # extraConfig = ''
       #   IdleAction=sleep
       #   IdleActionSec=20min
       # '';
     };
     openssh = {
       enable = true;
       settings = {
         PasswordAuthentication = false;
         KbdInteractiveAuthentication = false;
       };
     };
     pipewire = {
       enable = true;
       alsa.enable = true;
       pulse.enable = true;
       jack.enable = true;
     };
     tailscale = {
       enable = true;
       extraDaemonFlags = [
         "--no-logs-no-support"
       ];
       extraUpFlags = [
         "--login-server"
         "https://hs.triplehelix.log:443"
       ];
     };
     tzupdate.enable = true;
     # m8 + brave
     udev.extraRules = ''
       SUBSYSTEM=="usb" ATTRS{idVendor}=="16c0", ATTR{idProduct}=="048a", MODE="0660", GROUP="dialout"
     '';
     upower.enable = true;

     greetd = {
      # reenable after zen gets merged
      enable = false;
      vt = 3;
      settings = {
        default_session = {
          user = "enmesh";
          command = "Hyprland";
        };
      };
    };

     syncthing = {
       enable = false;
       settings = {
         devices = {
           "hajime" = { id = "TAJN7KP-OGXVNYQ-YBTQVGJ-UTSIDLO-2333N57-JZ4FBJC-NPCHIAN-KSKFVQW"; };
         };
         folders = {
           "default" = {
             path = "/storage";
             devices = ["hajime"];
             versioning = {
               type = "staggered";
               fsPath = "/storage/.stversions";
               params = {
                 cleanInterval = "3600";
                 maxAge = "31536000";
               };
             };
           };
         };
       };
     };

     xserver = {
       enable = false;
       desktopManager.gnome.enable = false;
       displayManager.gdm = {
         enable = false;
         wayland = false;
      };
     };
   };

   systemd.services = {
     charge-limiter = {
       enable = true;
       description = "Limiting battery charge threshold";
       serviceConfig = {
         Type = "oneshot";
         ExecStart = "/bin/sh -c 'echo 80 | tee /sys/class/power_supply/macsmc-battery/charge_control_end_threshold'";
       };
       after = [ "multi-user.target" ];
       wantedBy = [ "multi-user.target" ];
     };
     zfs-mount.enable = true;
   };

   users = {
      users.enmesh = {
        isNormalUser = true;
        description = "enmesh";
        extraGroups = [ "networkmanager" "wheel" "dialout" "storage" "i2c" "video" "input"];
        hashedPassword = "$y$j9T$k6rDM8jxlYk5or2V9zSJK/$.oL/VS97trDzszTolc2i1cJ5R4aymiTNEtqP9u1hZI2";
        packages = with pkgs; [];
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM+DVR15qVOhdW/eXvoHu5AL9YAvBoxQFPp6qV3uP9GL oddx@hajime"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF+D+6GlLDEKFQb9gVFzr3Uxqi++eO78NWOcmOCQhU4t enmesh@iphone"
       ];
        shell = pkgs.zsh;
     };
     users.root.hashedPassword = "$y$j9T$RMrEW7l9VMD6tcXo3AL1./$IzrRSimm8yIbWr.jyCUWTKheBiFPEbiN1TLutUAna81";
   };

   programs = {
     firefox.enable = true;
     geary.enable = true;
     hyprland.enable = true;
     river.enable = false;
   };

   system.stateVersion = "23.11";

   nix = {
      package = pkgs.nixVersions.git;
      extraOptions = ''
         experimental-features = nix-command flakes
      '';
      gc = {
         automatic = true;
      };
      settings = {
         auto-optimise-store = true;
         trusted-users = ["enmesh"];
      };
   };

   nixpkgs = {
      config = {
        allowUnfree = true;
        allowNonFree = true;
        allowUnsupportedSystem = true;
        firefox.enableWideVine = true;
      };
      overlays = [ 
        (import ./overlays/widevine.nix)
      ];
   };

   environment = {
     sessionVariables.MOZ_GMP_PATH = [ "${pkgs.widevine}/gmp-widevinecdm/system-installed" ];
     shells = [pkgs.bashInteractive pkgs.zsh];
     systemPackages = with pkgs; [
       exfat
       helix
       zfs
     ];
   };

   fonts = {
      fontDir.enable = true;
      packages = with pkgs; [
         fira
         fira-code
         ibm-plex
         mononoki
         roboto
         # nerd-fonts.0xproto
         nerd-fonts._3270
         nerd-fonts.agave
         nerd-fonts.anonymice
         nerd-fonts.arimo
         nerd-fonts.aurulent-sans-mono
         nerd-fonts.bigblue-terminal
         nerd-fonts.bitstream-vera-sans-mono
         nerd-fonts.blex-mono
         nerd-fonts.caskaydia-cove
         nerd-fonts.caskaydia-mono
         nerd-fonts.code-new-roman
         nerd-fonts.comic-shanns-mono
         nerd-fonts.commit-mono
         nerd-fonts.cousine
         nerd-fonts.d2coding
         nerd-fonts.daddy-time-mono
         nerd-fonts.departure-mono
         nerd-fonts.dejavu-sans-mono
         nerd-fonts.droid-sans-mono
         nerd-fonts.envy-code-r
         nerd-fonts.fantasque-sans-mono
         nerd-fonts.fira-code
         nerd-fonts.fira-mono
         nerd-fonts.geist-mono
         nerd-fonts.go-mono
         nerd-fonts.gohufont
         nerd-fonts.hack
         nerd-fonts.hasklug
         nerd-fonts.heavy-data
         nerd-fonts.hurmit
         nerd-fonts.im-writing
         nerd-fonts.inconsolata
         nerd-fonts.inconsolata-go
         nerd-fonts.inconsolata-lgc
         nerd-fonts.intone-mono
         nerd-fonts.iosevka
         nerd-fonts.iosevka-term
         nerd-fonts.iosevka-term-slab
         nerd-fonts.jetbrains-mono
         nerd-fonts.lekton
         nerd-fonts.liberation
         nerd-fonts.lilex
         nerd-fonts.martian-mono
         nerd-fonts.meslo-lg
         nerd-fonts.monaspace
         nerd-fonts.monofur
         nerd-fonts.monoid
         nerd-fonts.mononoki
         nerd-fonts.mplus
         nerd-fonts.noto
         nerd-fonts.open-dyslexic
         nerd-fonts.overpass
         nerd-fonts.profont
         nerd-fonts.proggy-clean-tt
         nerd-fonts.recursive-mono
         nerd-fonts.roboto-mono
         nerd-fonts.shure-tech-mono
         nerd-fonts.sauce-code-pro
         nerd-fonts.space-mono
         nerd-fonts.symbols-only
         nerd-fonts.terminess-ttf
         nerd-fonts.tinos
         nerd-fonts.ubuntu
         nerd-fonts.ubuntu-mono
         nerd-fonts.ubuntu-sans
         nerd-fonts.victor-mono
         nerd-fonts.zed-mono
      ];
   };

   # virtualisation.anbox.enable = true;

   programs.light.enable = true;
   programs.zsh.enable = true;
}
