{ pkgs, ... }:
{
  services.swayidle = {
    enable = true;
    events = [
      { event = "before-sleep"; command = "pidof hyprlock || ${pkgs.hyprlock}/bin/hyprlock"; }
    ];
    timeouts = [
      # dim screen after 5 mins
      { timeout = 300; command = "brightnessctl -s set 10"; resumeCommand = "brightnessctl -r"; }
      # lock after 7 mins
      { timeout = 420; command = "pidof hyprlock || ${pkgs.hyprlock}/bin/hyprlock"; }
      # display off after 12 mins
      { timeout = 720; command = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off"; resumeCommand = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on"; }
      # sleep after 20 mins
      # { timeout = 1200; command = "systemctl hybrid-sleep"; }
    ];
  };
}
