{ pkgs, ... }:
{
  programs.zsh = {
    enable = true;
    initExtra = ''
      set -o emacs

      fmt -80 ~/.motd.txt
      alias nixgrade="doas nixos-rebuild switch --flake ~/.config/nixos/"
      alias streamup="gphoto2 --stdout --capture-movie | ffmpeg -i - -vcodec rawvideo -pix_fmt yuv420p -f v4l2 /dev/video0"
      alias rip="yt-dlp -f bestaudio -x --audio-format mp3 --audio-quality 0 --add-metadata --embed-thumbnail -o \"%(artist)s - %(title)s.%(ext)s\""

      PS1='%B%F{cyan}%m:%b%F{white}%1~ %B%F{cyan}λ%f%F{magenta}=%f%b '
    '';
    loginExtra = ''
      if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
        exec Hyprland
      fi
    '';
  };
}
