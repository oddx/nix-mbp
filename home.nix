{
   pkgs,
   ...
}: {
   imports = [
     ./ags.nix
     ./alacritty.nix
     ./direnv.nix
     ./firefox.nix
     ./gammastep.nix
     ./git.nix
     ./helix.nix
     ./hypridle.nix
     ./hyprland.nix
     ./hyprlock.nix
     ./kanshi.nix
     ./mako.nix
     ./ssh.nix
     ./theme.nix
     ./tmux.nix
     ./zsh.nix
   ];

   home = {
     username = "enmesh";
     homeDirectory = "/home/enmesh";
     enableNixpkgsReleaseCheck = true;
     packages = with pkgs; [
       # cli
       ## languages
       bun
       gcc
       # ghc cabal-install
       gleam erlang_27 rebar3
       # ocaml
       python3
       rustup # for rustc linker
       sqlite

       ### lsps
       llvm
             
       ## utils
       alacritty
       brightnessctl
       btop
       curl
       ddate
       ddcutil
       direnv
       doas
       entr
       evcxr # replace with irust sometime
       git
       htop
       httpie
       id3v2
       jq
       kalker
       libqalculate
       mosh
       ollama
       openssh
       ripgrep
       tree
       tytools # m8 stuff
       unison
       unzip
       websocat
       zip

       ## audio
       ffmpeg_6-full

       ## video
       gphoto2
       imagemagick
       mpv
       v4l-utils

       # desktop
       ## apps
       airshipper
       ardour
       audacity
       # blender
       bespokesynth
       bruno
       ungoogled-chromium
       # equibop
       geeqie
       gimp
       gnome-calendar
       nautilus
       # heroic
       helvum
       # icon-library #find gnome icons
       kooha
       nicotine-plus
       obs-studio
       prismlauncher
       signal-desktop
       # supersonic
       transmission_4
       vesktop
       vscodium-fhs

       ## DE
       # ironbar
       mako
       # rofi-wayland
       # swayosd
       # ulauncher

       ### development
       # hoppscotch
       aseprite
       # godot_4

       ## utils
       cavalier
       adwaita-icon-theme
       # colloid-icon-theme
       gnome-disk-utility
       hypridle
       hyprpicker
       hyprshot
       kanshi
       kdePackages.kdenlive
       # kdenlive
       pavucontrol
       playerctl
       # qmk
       sassc
       swaybg
       swayimg
       xdg-utils
       yt-dlp
     ];

     sessionPath = ["$HOME/bin"];
     sessionVariables = {
        LC_ALL = "en_US.UTF-8";
        LESSHISTFILE = "/dev/null";
        VISUAL = "hx";
        XDG_DOWNLOAD_DIR = "$HOME/downloads/";
        XDG_PICTURES_DIR = "$HOME/pictures/";
        HYPRSHOT_DIR = "$HOME/pictures/shots/";
     };

     shellAliases = {
       ls = "ls -aFhl";
       motd = "$VISUAL $HOME/.motd.txt";
       todo = "$VISUAL $HOME/.todo.md";
       vi = "$VISUAL";
     };
     stateVersion = "23.11";
  };
}
