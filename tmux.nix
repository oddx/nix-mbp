{ pkgs, ... }:
{
  programs.tmux = {
    enable = true;
    clock24 = true;
    mouse = true;
    extraConfig = ''
      set -g default-terminal "alacritty" 
      set-option -sa terminal-overrides ",alacritty*:Tc"
    '';
  };
}
