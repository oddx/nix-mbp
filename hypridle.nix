{ pkgs, ... }:
{
  services.hypridle = {
    enable = true;
    settings = {
      general = {
          lock_cmd = "pidof ${pkgs.hyprlock}/bin/hyprlock || ${pkgs.hyprlock}/bin/hyprlock";
          before_sleep_cmd = "loginctl lock-session";
          after_sleep_cmd = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
      };

      listener = [
        {
            timeout = 300;
            on-timeout = "brightnessctl -s set 10";
            on-resume = "brightnessctl -r";
        }
        { 
            timeout = 420;
            on-timeout = "echo 0 > /sys/class/leds/kbd_backlight/brightness";
            on-resume = "echo 5 > /sys/class/leds/kbd_backlight/brightness";
        }
        { 
            timeout = 420;
            on-timeout = "${pkgs.mako}/bin/makoctl mode -s away";
            on-resume = "${pkgs.mako}/bin/makoctl mode -r away";
        }
        {
            timeout = 420;
            on-timeout = "loginctl lock-session";
        }
        {
            timeout = 720;
            on-timeout = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off";
            on-resume = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
        }
        # {
        #     timeout = 1200;
        #     on-timeout = "systemctl sleep";
        # }
      ];
    };
  #   events = [
  #     { event = "before-sleep"; command = "pidof hyprlock || ${pkgs.hyprlock}/bin/hyprlock"; }
  #   ];
  #   timeouts = [
  #     # dim screen after 5 mins
  #     { timeout = 300; command = "brightnessctl -s set 10"; resumeCommand = "brightnessctl -r"; }
  #     # lock after 7 mins
  #     { timeout = 420; command = "pidof hyprlock || ${pkgs.hyprlock}/bin/hyprlock"; }
  #     # display off after 12 mins
  #     { timeout = 720; command = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off"; resumeCommand = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on"; }
  #     # sleep after 20 mins
  #     # { timeout = 1200; command = "systemctl hybrid-sleep"; }
  #   ];
  };
}
