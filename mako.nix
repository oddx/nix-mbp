{
  services.mako = {
    enable = true;
    anchor = "top-right";
    backgroundColor = "#282828";
    borderColor = "#3584E4";
    borderRadius = 5;
    borderSize = 2;
    defaultTimeout = 7500;
    extraConfig = ''
      [mode=dnd]
      invisible=1

      [mode=away]
      default-timeout=0
      ignore-timeout=1
    '';
    font = "Roboto";
    format = ''<span font="weight=500 Italic">%s ✨</span>\n<span size="10000">%b</span>'';
    height = 175;
    layer = "overlay";
    margin = "8";
    padding = "10";
    textColor = "#c6c6c6";
    width = 350;
  };
}
