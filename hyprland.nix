{ config, pkgs, ... }:
let
  plugins = config.roadrunner.inputs.hyprland-plugins.packages.${pkgs.system};
in
{
  wayland.windowManager.hyprland = {
    enable = true;
    #plugins = with plugins; [ hyprbars ];
    # "HDMI-A-1, 2560x1440@60, 0x0, 1"
    settings = {
      # monitor = [
      #   "HDMI-A-1, 3440x1440@85, 0x0, 1"
      #   "eDP-1, 3024x1890@60, 3440x0, 1.575"
      #   ",preferred,auto,1"
      # ];

      input = {
        follow_mouse = 1;
        sensitivity = 1;
        natural_scroll = true;
        repeat_delay = 200;
        repeat_rate = 75;

        touchpad = {
          disable_while_typing = true;
          natural_scroll = true;
          tap-and-drag = true;
        };
      };

      misc = {
        disable_splash_rendering = true;
        disable_hyprland_logo = true;
        #force_hypr_chan = false;
        force_default_wallpaper = 0;
        vrr = 1;
      };
      
      general = {
        gaps_in = 15;
        gaps_out = 30;
        border_size = 5;
        
        "col.active_border" = "rgba(F34B7DFF) rgba(3584E4FF) 45deg";
        "col.inactive_border" = "rgba(66333333)";

        resize_on_border = true;
      };

      cursor = {
        inactive_timeout = 10;
      };

      decoration = {
        rounding = 10;
        blur = {
          enabled = false;
          size = 8;
          passes = 3;
          new_optimizations = "on";
          noise = 0.01;
          contrast = 0.9;
          brightness = 0.8;
        };
      };

      animations = {
        enabled = true;
        # bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
        animation = [
          "windows, 1, 7, default"
          #"windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "fade, 1, 10, default"
          "workspaces, 1, 6, default"
        ];
      };

      dwindle = {
        pseudotile = false;
      };

      binds = {
        allow_workspace_cycles = true;
      };

      gestures = {
        workspace_swipe = true;
      };

      windowrule = [
        "size 750 1000, ^(Brave)"
        "size 750 1000, ^(firefox)"
        "size 750 1000, ^(zen)"
        "size 750 250, ^(Alacritty)"
        "size 1000 500, ^(geary)"
        "move onscreen cursor -50% -50%,^(firefox)"
        "move onscreen cursor -50% -50%,^(Alacritty)"
      ];

      windowrulev2 = "float, class:.*";

      bind = let
        binding = mod: cmd: key: arg: "${mod}, ${key}, ${cmd}, ${arg}";
        ws = binding "SUPER" "workspace";
        mvtows = binding "SUPER ALT" "movetoworkspace";
        arr = [1 2 3 4 5 6 7 8 9];
      in [
        "SUPER, Space,  exec, ags -t launcher"
        "SUPER, Return, exec, alacritty"

        "SUPER, Tab, focuscurrentorlast"
        "SUPER SHIFT, Q, exit"
        "SUPER, Q, killactive"
        "SUPER, I, togglefloating"
        "SUPER, M, fullscreen"
        "SUPER, T, workspaceopt, allfloat"

        "SUPER, S, exec, hyprshot -m region -o"
        "SUPER+SHIFT, S, exec,hyprshot -m window -o"
        "SUPER+ALT, S, exec,hyprshot -m output -o"
        "SUPER, L, exec, hyprlock"

        ",XF86AudioPrev, exec, playerctl previous"
        ",XF86AudioPlay, exec, playerctl play-pause"
        ",XF86AudioNext, exec, playerctl next"

        "SUPER+ALT, U, movetoworkspace, 1"
        "SUPER+ALT, I, movetoworkspace, 2"
        "SUPER+ALT, O, movetoworkspace, 3"

        "SUPER+ALT, H, movefocus, l"
        "SUPER+ALT, L, movefocus, r"
        "SUPER+ALT, K, movefocus, u"
        "SUPER+ALT, J, movefocus, d"

        "SUPER+SHIFT, H, movewindow, l"
        "SUPER+SHIFT, L, movewindow, r"
        "SUPER+SHIFT, K, movewindow, u"
        "SUPER+SHIFT, J, movewindow, d"

        "SUPER+CTRL, H, resizeactive, -100 0"
        "SUPER+CTRL, L, resizeactive, 100 0"
        "SUPER+CTRL, K, resizeactive, 0 -100"
        "SUPER+CTRL, J, resizeactive, 0 100"

        (ws "left" "e-1")
        (ws "right" "e+1")
        (mvtows "left" "e-1")
        (mvtows "right" "e+1")
      ]
      ++ (map (i: ws (toString i) (toString i)) arr)
      ++ (map (i: mvtows (toString i) (toString i)) arr);
      # add binds for uiojklnm,

      binde = [
        ",XF86MonBrightnessUp, exec, brightnessctl set +1%"
        ",XF86MonBrightnessDown, exec, brightnessctl set 1%-"
        ",XF86AudioRaiseVolume, exec, wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+"
        ",XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
      ];

      bindm = [
        "SUPER, mouse:272, movewindow"
      ];

      env = [
        "AGS_SKIP_V_CHECK, true"
        # "HYPRCURSOR_THEME,Bibata-Modern-Classic"
        # "HYPRCURSOR_SIZE,24 "
        # "HYPRLAND_LOG_WLR, true"
      ];

      debug = {
        disable_logs = true;
      };

      exec-once = [
        "swaybg -i ~/pictures/papes/wallpaper"
        "${pkgs.mako}/bin/mako"
        "ags"
        "hyprctl setcursor Bibata-Modern-Classic 24"
        # "geary &; sleep 1 && hyprctl dispatch closewindow \(geary\)"
        # "hyprlock"
      ];
    };
  };
}
