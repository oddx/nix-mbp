{ pkgs, ... }:
{
  services.kanshi = {
    enable = true;
    systemdTarget = "hyprland-session.target";
    settings = [
      {
        profile.name = "docked"; 
        profile.exec = [
          "pkill ags && ags"
          "pkill swaybg && swaybg -i /home/enmesh/pictures/papes/wallpaper-uw"
          "echo 0 > /sys/class/leds/kbd_backlight/brightness"
        ];
        profile.outputs = [
          {
            criteria = "HDMI-A-1";
            mode = "3440x1440@85Hz";
            position = "0,0";
            adaptiveSync = true;
          }
          {
            criteria = "eDP-1";
            status = "disable";
          }
        ];
      }
      {
        profile.name = "undocked";
        # remove manual disable when monitor bug is fixed
        profile.exec = [
          "hyprctl keyword monitor 'eDP-1, enable'"
          "pkill ags && ags"
          "pkill swaybg && swaybg -i /home/enmesh/pictures/papes/wallpaper"
          "echo 5 > /sys/class/leds/kbd_backlight/brightness"
        ];
        profile.outputs = [{
          criteria = "eDP-1";
          mode = "3024x1890@60Hz";
          scale = 1.575;
          position = "0,0";
          status = "enable";
        }];
      }
      {
        profile.name = "default"; 
        profile.exec = [ 
          "pkill ags && ags" 
          "pkill swaybg && swaybg -i /home/enmesh/pictures/papes/wallpaper"
          "echo 0 > /sys/class/leds/kbd_backlight/brightness"
        ];
        profile.outputs = [
          {
            criteria = "*";
            position = "0,0";
          }
          {
            criteria = "eDP-1";
            status = "disable";
          }
        ];
      }
    ];
  };
}

# profile {
#   output eDP-1 disable
#   output HDMI-A-1 enable
# }

# profile {
#   output eDP-1 enable
# }
