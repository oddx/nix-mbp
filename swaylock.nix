{ pkgs, ... }:
{
  programs.swaylock = {
    enable = true;
    settings = {
      #color = "1C2023";
      image = "~/pictures/papes/sky-beach.png";
    };
  };
}
