{ pkgs, ... }:
{
  programs.git = {
    enable = true;
    userEmail = "sacred.buckshot298@aleeas.com";
    userName = "Alex Platz";
    extraConfig.core.defaultBranch = "main";
  };
}
