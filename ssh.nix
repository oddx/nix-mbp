{ pkgs, ... }:
{
  programs.ssh = {
    enable = true;
    extraConfig = ''
      Host hajime
        HostName hajime
        User oddx
    '';
  };
}
