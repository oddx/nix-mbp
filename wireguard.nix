{ pkgs, ... }: {
  networking.wireguard.interfaces = let
    # [Peer] section -> Endpoint
    server_ip = "193.37.254.178";
  in {
    wg0 = {
      # [Interface] section -> Address
      ips = [ "10.2.0.2/32" ];

      # [Peer] section -> Endpoint:port
      listenPort = 51820;

      # Path to the private key file.
      privateKeyFile = "/etc/protonvpn-az-30.key";

      peers = [{
        # [Peer] section -> PublicKey
        publicKey = "nSo/1FlBPdm/hotIKPb2dFcY5AwZPQPBcbvLdcL6Zw4=";
        # [Peer] section -> AllowedIPs
        allowedIPs = [ "0.0.0.0/0" ];
        # [Peer] section -> Endpoint:port
        endpoint = "${server_ip}:51820";
        persistentKeepalive = 25;
      }];
    };
  };
}

