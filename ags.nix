{ pkgs, ... }:
{
  programs.ags = {
    enable = true;
    configDir = ./ags;
    extraPackages = with pkgs; [ 
      dart-sass
      fd
      swaylock
    ];
  };
}
